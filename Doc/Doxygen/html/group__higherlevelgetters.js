var group__higherlevelgetters =
[
    [ "is_active", "group__higherlevelgetters.html#gaa0ab4d6a754a23ea13664a553bcc8ff2", null ],
    [ "is_driving", "group__higherlevelgetters.html#ga33dcd96e9b12dec794c56be85ec1ee05", null ],
    [ "set_active", "group__higherlevelgetters.html#ga415c69930f291d5e06b7211b31843310", null ],
    [ "set_inactive", "group__higherlevelgetters.html#ga62f1c0eea56e27d0853cb58f30eb140d", null ],
    [ "stopped_at_min_distance", "group__higherlevelgetters.html#gae8b8c0ab24ccb572281785aeca8541e1", null ],
    [ "turn_left_for_ms", "group__higherlevelgetters.html#gaf04fd16ca0e2ace656f9549c43d16459", null ],
    [ "turn_right_for_ms", "group__higherlevelgetters.html#gac0698f02f6a21d9d1f5b9cf2820306cf", null ]
];