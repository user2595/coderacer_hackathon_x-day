// the compiler switch for an ESP8266 is looking like this: #elif defined(ARDUINO_ARCH_ESP8266)
#include "CodeRacer.h"

/**	@brief CodeRace constructor without pins. All pins settings taken from the coderacer header file 
*		@return nothing
*/
CodeRacer::CodeRacer()
{
	_button_pin = H_BUTTON_PIN;
	_servo_pin = H_SERVO_PIN;
	_us_trigger_pin = H_US_TRIG_PIN;
	_us_echo_pin = H_US_ECHO_PIN;
	_drive_left_frwd_pin = H_DRIVE_LEFT_FWRD_PIN;
	_drive_left_back_pin = H_DRIVE_LEFT_BACK_PIN;
	_drive_left_enable_pin = H_DRIVE_LEFT_ENABLE_PIN;
	_drive_right_frwd_pin = H_DRIVE_RIGHT_FWRD_PIN;
	_drive_right_back_pin = H_DRIVE_RIGHT_BACK_PIN;
	_drive_right_enable_pin = H_DRIVE_RIGHT_ENABLE_PIN;
	_led_frwd_pin = H_LED_FRWD_PIN;
	_led_stop_pin = H_LED_STOP_PIN;
	_led_left_pin = H_LED_LEFT_PIN;
	_led_right_pin = H_LED_RIGHT_PIN;
}

/**	@brief CodeRace constructor with pins.This will overwrite the default settings taken from the header file.
*		@param	button_pin							Pin the external button is connected at
*		@param	servo_pin								Pin the servo drive is connected at
*		@param	us_trigger_pin					Pin the trigger signal of the ultrasonic sensor is connected at
*		@param	us_echo_pin							Pin the echo signal of the ultrasonic sensor is connected at
*		@param	drive_left_frwd_pin			Pin the forward pin of the left side drive device driver is connected at
*		@param	drive_left_back_pin			Pin the backward pin of the left side drive device driver is connected at
*		@param	drive_left_enable_pin		Pin the enable pin of the left side drive device driver is connected at
*		@param	drive_right_frwd_pin		Pin the forward pin of the right side drive device driver is connected at
*		@param	drive_right_back_pin		Pin the backward pin of the right side drive device driver is connected at
*		@param	drive_right_enable_pin	Pin the enable pin of the right side drive device driver is connected at
*		@param	led_frwd_pin						Pin the led that signals forward direction is connected at
*		@param	led_stop_pin						Pin the led that signals that the drives are stopped is connected at
*		@param	led_left_pin						Pin the led that signals left side direction is connected at
*		@param	led_right_pin						Pin the led that signals right side direction is connected at
*		@return nothing
*/
CodeRacer::CodeRacer(uint8_t button_pin , uint8_t 	servo_pin,
	uint8_t us_trigger_pin, uint8_t us_echo_pin,
	uint8_t drive_left_frwd_pin, uint8_t drive_left_back_pin, uint8_t drive_left_enable_pin,
	uint8_t drive_right_frwd_pin, uint8_t drive_right_back_pin, uint8_t drive_right_enable_pin,
	uint8_t led_frwd_pin, uint8_t led_stop_pin, uint8_t led_left_pin, uint8_t led_right_pin
)
{
	_button_pin = button_pin;
	_servo_pin = servo_pin;
	_us_trigger_pin = us_trigger_pin;
	_us_echo_pin = us_echo_pin;
	_drive_left_frwd_pin = drive_left_frwd_pin;
	_drive_left_back_pin = drive_left_back_pin;
	_drive_left_enable_pin = drive_left_enable_pin;
	_drive_right_frwd_pin = drive_right_frwd_pin;
	_drive_right_back_pin = drive_right_back_pin;
	_drive_right_enable_pin = drive_right_enable_pin;
	_led_frwd_pin = led_frwd_pin;
	_led_stop_pin = led_stop_pin;
	_led_left_pin = led_left_pin;
	_led_right_pin = led_right_pin;
}

/**	@brief Initialisation of all attributes and settings of the coderacer. Defaults are taken from the header file.
*		@return	nothing
*/
void CodeRacer::begin() {

	// init of variables and objects

	_servo_dummy = new Servo();		// the dummy is needed so far to avoid conflicts with analog write
	_servo = new Servo();
	servo_center_pos = H_SERVO_CENTER_POS;
	servo_left_pos = H_SERVO_LEFT_POS;
	servo_right_pos = H_SERVO_RIGHT_POS;
	servo_sweep_left_pos = H_SERVO_SWEEP_LEFT_POS;
	servo_sweep_right_pos = H_SERVO_SWEEP_RIGHT_POS;
	_servo_position = servo_center_pos;
	_servo_sweep_step = SERVO_SWEEP_TO_LEFT_STEP;
	_servo_position_set_at_ms = millis();
	_servo_position_eta_in_ms = 0;

	_drive_left_speed = H_DRIVE_LEFT_SPEED;
	_drive_right_speed = H_DRIVE_RIGHT_SPEED;

	_turn_left_for_ms = H_RACER_TURN_LEFT_FOR_MS;
	_turn_right_for_ms = H_RACER_TURN_RIGHT_FOR_MS;

	coderracer_activ = false;
	_coderracer_activ = true;
	_drive = false;
	_drive_set_at_ms = millis();
	_servo_sweep = false;

	_last_led_switched_at_ms = millis();
	_last_led_on = 0;
	_led_count = 3;

	_servo_look_around_at_ms = millis() + random(FUN_MIN_PAUSE_MS, FUN_MAX_PAUSE_MS);

	_usonic_stop_distance_cm = H_US_STOP_DISTANCE_CM;
	usonic_set_stop_distance_cm(_usonic_stop_distance_cm);
	_coderacer_stopped_at_min_distance = false;

	// Ultrasonic sensor
	pinMode(_us_trigger_pin, OUTPUT);
	pinMode(_us_echo_pin, INPUT);
	
	// Servo drive
	_servo->attach(_servo_pin);

	// Left drive
	pinMode(_drive_left_frwd_pin, OUTPUT);
	pinMode(_drive_left_back_pin, OUTPUT);
	set_drive_left_state(DRIVESTOP);
	ledcSetup(DRIVE_PWM_LEFT_CHANNEL, 5000, 8);													// channel , 50 Hz, 8-bit width
	ledcAttachPin(_drive_left_enable_pin, DRIVE_PWM_LEFT_CHANNEL);			// connect left drive enable with PWM channel

	// Right drive
	pinMode(_drive_right_frwd_pin, OUTPUT);
	pinMode(_drive_right_back_pin, OUTPUT);
	set_drive_right_state(DRIVESTOP);
	ledcSetup(DRIVE_PWM_RIGHT_CHANNEL, 5000, 8);												// channel , 50 Hz, 8-bit width
	ledcAttachPin(_drive_right_enable_pin, DRIVE_PWM_RIGHT_CHANNEL);		// connect right drive enable pin with PWM channel

	// LEDs
	pinMode(_led_frwd_pin, OUTPUT);
	pinMode(_led_stop_pin, OUTPUT);
	pinMode(_led_left_pin, OUTPUT);
	pinMode(_led_right_pin, OUTPUT);
	// all LEDS off
	set_leds_all_off();

	// Button & -interrupt
	button_last_pressed_at_ms = 0;
	pinMode(_button_pin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(_button_pin), _set_button_state, FALLING);

	// Random
	randomSeed(analogRead(0));

	//fun stuff
	coderacer_fun_enabled = false;

}

//**************************************
//*** Coderacer hihger level methods ***
//**************************************
/**	@defgroup higherlevel Higher level methods, setters and getters
*		@{
*/
/**	@defgroup higherlevelmeths Methods
*		@{
*/

/**	@brief Stops the racer and sets status LEDs.
*		@return	nothing
*/
void CodeRacer::stop_driving() {
	_servo_sweep = false;
	_drive = false;
	set_drives_stop_left_right();
	set_leds_left_stop_frwd_right(LEDOFF, LEDON, LEDOFF, LEDOFF);
}

/**	@brief Sets the speed and the directions of both drives so that it will move forward.
*
*		The speed is taken from the header file or set by one of the methods defined in the lower level drive methods section
*		@return	nothing
*/
void CodeRacer::drive_forward()
{
	drive_forward(_drive_left_speed, _drive_right_speed);
}

/**	@brief Sets the speed as specified for both drives and the directions of both drives so that it will move forward.
*
*		The specified speed values for both drives will be stored internaly so next time if you use e.g. drive_forward() exactly the here specified values will be taken.
*		@param left_speed speed for the left side drive. 0<=speed<=255
*		@param right_speed speed for the right side drive. 0<=speed<=255
*		@return	nothing
*/
void CodeRacer::drive_forward(uint8_t left_speed, uint8_t right_speed)
{
	set_drives_states_left_right(DRIVEFRWD, DRIVEFRWD);
	set_drives_speed_left_right(left_speed, right_speed);
	set_leds_left_stop_frwd_right(LEDOFF, LEDOFF, LEDON, LEDOFF);
	_drive = true;
	_drive_set_at_ms = millis();
}

/**	@brief Sets the speed and the directions of both drives so that it will move backward.
*
*		The speed is taken from the header file or set by one of the methods defined in the lower level drive methods section
*		@return	nothing
*/
void CodeRacer::drive_backward()
{
	drive_backward(_drive_left_speed, _drive_right_speed);
}

/**	@brief Sets the speed as specified for both drives and the directions of both drives so that it will move backward.
*
*		The specified speed values for both drives will be stored internaly so next time if you use e.g. drive_backward() exactly the here specified values will be taken.
*		@param left_speed speed for the left side drive. 0<=speed<=255
*		@param right_speed speed for the right side drive. 0<=speed<=255
*		@return	nothing
*/
void CodeRacer::drive_backward(uint8_t left_speed, uint8_t right_speed)
{
	set_drives_states_left_right(DRIVEBACK, DRIVEBACK);
	set_drives_speed_left_right(left_speed, right_speed);
	set_leds_left_stop_frwd_right(LEDOFF, LEDON, LEDON, LEDOFF);
	_drive = true;
	_drive_set_at_ms = millis();
}

/**	@brief Will turn the racer to the left for the internally stroe time in ms and with the internally stored speed.
*
*		The speed for both the left side and right side drive can be set by the methods described in the lower level drive section. The turn to left time can be set by thoose methods as well. 
*		The method is delayed until the racer has turned.
*		@return	nothing
*/
void CodeRacer::turn_left()
{
	turn_left(_turn_left_for_ms, _drive_left_speed, _drive_right_speed);
}

/**	@brief Will turn the racer to the left for the specified time in ms and with the internally stored speed.
*
*		The specified duration of time is stored internally and will be used next time by e.g. turn_left() 
*		@param turn_for_ms duration of time in ms to turn the racer
*		@return	nothing
*/
void CodeRacer::turn_left(unsigned long turn_for_ms)
{
	turn_left(turn_for_ms, _drive_left_speed, _drive_right_speed);
}

/**	@brief Will turn the racer to the left for the specified time in ms and with the specified speed.
*
*		The specified duration of time and the specified speeds are stored internally and will be used next time by e.g. turn_left() 
*		@param turn_for_ms duration of time in ms to turn the racer
*		@param left_speed speed for the left side drive
*		@param right_speed speed for the right side drive
*		@return	nothing
*/
void CodeRacer::turn_left(unsigned long turn_for_ms, uint8_t left_speed, uint8_t right_speed)
{
	_drive = false;
	set_leds_left_stop_frwd_right(LEDON, LEDOFF, LEDOFF, LEDOFF);	// LEDs setzen
	set_drives_states_left_right(DRIVEBACK, DRIVEFRWD);
	set_drives_speed_left_right(left_speed, right_speed);
	// wait set delay for the timed turn
	_turn_left_for_ms = turn_for_ms;
	delay(_turn_left_for_ms);
	// stop drives again
	set_drives_stop_left_right();
}

/**	@brief Will turn the racer to the right for the internally stroe time in ms and with the internally stored speed.
*
*		The speed for both the left side and right side drive can be set by the methods described in the lower level drive section. The turn to right time can be set by thoose methods as well.
*		The method is delayed until the racer has turned.
*		@return	nothing
*/
void CodeRacer::turn_right()
{
	turn_right(_turn_right_for_ms, _drive_left_speed, _drive_right_speed);
}

/**	@brief Will turn the racer to the right for the specified time in ms and with the internally stored speed.
*
*		The specified duration of time is stored internally and will be used next time by e.g. turn_right()
*		@param turn_for_ms duration of time in ms to turn the racer
*		@return	nothing
*/
void CodeRacer::turn_right(unsigned long turn_for_ms)
{
	turn_right(turn_for_ms, _drive_left_speed, _drive_right_speed);
}

/**	@brief Will turn the racer to the right for the specified time in ms and with the specified speed.
*
*		The specified duration of time and the specified speeds are stored internally and will be used next time by e.g. turn_right()
*		@param turn_for_ms duration of time in ms to turn the racer
*		@param left_speed speed for the left side drive
*		@param right_speed speed for the right side drive
*		@return	nothing
*/
void CodeRacer::turn_right(unsigned long turn_for_ms, uint8_t left_speed, uint8_t right_speed)
{
	_drive = false;
	set_leds_left_stop_frwd_right(LEDOFF, LEDOFF, LEDOFF, LEDON);		// LEDs setzen
	set_drives_states_left_right(DRIVEFRWD, DRIVEBACK);
	set_drives_speed_left_right(left_speed, right_speed);
	// wait set delay for the timed turn
	_turn_right_for_ms = turn_for_ms;
	delay(_turn_right_for_ms);
	// stop drives again
	set_drives_stop_left_right();
}

/**	@brief	Enables to stopp the racer if during a distance measurement the measured distance is smaller then the internally stored minimal distance.
*
*		This allow all of the none-single shot ultra sonic measurement methods to stopp the racer in the case the measured distance is smaller than minimal distance. This is just the enablement - you have to call one of the none-single-shot ultra sonic measurement methods continously while driving and maybe sweeping the servo.
*		If the racer was stopped can be checked with stopped_at_min_distance() - it will return true in that case.
*		The minimal distance can be set by the ultrasonic sensor setter methods.
*		There is an example coming with the coderacer libary that you can load and get an idea how that works.
*		@return	nothing
*/
void CodeRacer::start_stop_at_min_distance() {
	start_stop_at_min_distance(_usonic_stop_distance_cm);
}

/**	@brief Enables to stopp the racer if during a distance measurement the measured distance is smaller then the specified minimal distance.
*
*		This is almos the same as described for start_stop_at_min_distance(). You can specify the distance in cm here - this value will be stored internally and used by start_stop_at_min_distance() next time.
*		@param min_distance_cm the minimal disctance in cm
*		@return	nothing
*/
void CodeRacer::start_stop_at_min_distance(unsigned long min_distance_cm) {
	if (false == _coderacer_stop_at_distance_enabled) {
		_coderacer_stopped_at_min_distance = false;
		usonic_set_stop_distance_cm(min_distance_cm);
		_coderacer_stop_at_distance_enabled = true;
	}
}

/**	@brief Disables to stopp the racer if during a distance measurement the measured distance is smaller then the specified minimal distance.
*		@return	nothing
*/
void CodeRacer::stop_stop_at_min_distance() {
	_coderacer_stop_at_distance_enabled = false;
}

/**	@brief This will return if the codracer is in active mode or not.
*
*		There is a button used to toogle between active and inactive each time it is pressed This may help you to start driving and scanning the distance by pressing the button - and as well to stop the racer by pressing the button.
*		This method will set the LEDs depending on the mode and sets the servo back to center and stopps the racer when leaving the active mode. You can leave or enter the active mode as well by setting using set_active() and set_inactive().
*		The button itself triggers and internall interrupt event and sets the active state - but you have to continously call that method to switch between inactive and active mode depending on the active state. 
*		If in inactive mode and fun is enabeld by just setting the coderacer_fun_enabled = true ... some fun will happen :-)
* 
*		@return	True if the coderacer is in active mode. False if in inactive mode.
*/
bool CodeRacer::start_stop() {
	if (_coderracer_activ != coderracer_activ) {
		_coderracer_activ = coderracer_activ;
		if (true == _coderracer_activ) {
			set_leds_all_off();
			delay(500);
		}
		else {
			stop_driving();
			set_leds_all_on();
			delay(200);
			servo_set_to_center();
			_servo_look_around_at_ms = millis() + random(20000, 120000);
		}
	}

	if ((false == _coderracer_activ) && (true == coderacer_fun_enabled)) {
		kitt();
		look_around();
	}
		
	return(_coderracer_activ);
}

/**	@} */ // end of group higherlevelmeths
					/**	@defgroup higherlevelgetters Getters and setters
					*		@{
					*/

/**	@brief Returns true if the racer was stopped at minimum distance. This set to false each time start_stop_at_min_distance() is called.
*		@return	True if stopped.
*/
bool CodeRacer::stopped_at_min_distance() {
	return(_coderacer_stopped_at_min_distance);
}

/**	@brief Return true if the racer is driving - forward or backward
*		@return	True if driving forward or backward
*/
bool CodeRacer::is_driving() {
	return(_drive);
}

/**	@brief Returns the duration of time that is internally stored and used for turning the racer left
*		@return	Time to turn left in ms
*/
unsigned long CodeRacer::turn_left_for_ms() {
	return(_turn_left_for_ms);
}

/**	@brief Returns the duration of time that is internally stored and used for turning the racer left
*		@return	Time to turn left in ms
*/
unsigned long CodeRacer::turn_right_for_ms() {
	return(_turn_right_for_ms);
}

/**	@brief Sets the coderracer_active state to inactive. If start_stop() is used - the inactive mode will be entered.
*		@return	nothing
*/
void CodeRacer::set_inactive() {
	coderracer_activ = false;
}

/**	@brief Sets the coderracer_active state to active. If start_stop() is used - the active mode will be entered.
*		@return	nothing
*/
void CodeRacer::set_active() {
	coderracer_activ = true;
}

/**	@brief Checks if coderracer_activ is set.
*
*		coderracer_activ is toggled each time the button is pressed. After power on coderracer_activ is set to False.
*		@return	True id coderracer_activ is set - False if not.
*/
bool CodeRacer::is_active() {
	return(coderracer_activ);
}

/**	@} */ // end of group higherlevelgetters
/**	@} */ // end of group higherlevel

//**************************************
//*** Just for fun									 ***
//**************************************
/**	@defgroup lowerlevelfun Lower level fun stuff methods
*		@{
*/
/**	@brief Fun stuff - will move the servo around after a random amount of time
*		@return	nothing
*/
void CodeRacer::look_around()
{
	if (_servo_look_around_at_ms < millis()) {
		_servo_look_around_at_ms = millis() + random(FUN_MIN_PAUSE_MS, FUN_MAX_PAUSE_MS);
		servo_set_to_left();
		delay(500);
		servo_set_to_right();
		delay(800);
		servo_set_to_center();
		delay(300);
		servo_set_to_left();
		delay(100);
		servo_set_to_center();
	}
}

/**	@brief Fun stuff - you know Knightrider...
*		@return	nothing
*/
void CodeRacer::kitt()
{
	if (millis() - _last_led_switched_at_ms > LED_SWITCH_MS) {
		_last_led_switched_at_ms = millis();
		if (_last_led_on >= 5) {
			_led_count = -1;
		}
		else if (_last_led_on <= 0) {
			_led_count = 1;
		}
		_last_led_on = _last_led_on + _led_count;
		switch (_last_led_on) {
		case 0:
			set_leds_left_stop_frwd_right(LEDON, LEDOFF, LEDOFF, LEDOFF);
			break;
		case 1:
			set_leds_left_stop_frwd_right(LEDON, LEDOFF, LEDOFF, LEDOFF);
			break;
		case 2:
			set_leds_left_stop_frwd_right(LEDOFF, LEDON, LEDOFF, LEDOFF);
			break;
		case 3:
			set_leds_left_stop_frwd_right(LEDOFF, LEDOFF, LEDON, LEDOFF);
			break;
		case 4:
		case 5:
			set_leds_left_stop_frwd_right(LEDOFF, LEDOFF, LEDOFF, LEDON);
			break;
		}
	}
}

/**	@} */ // end of group lowerlevelfun

//**************************************
//*** Servo drive lower level control ***
//**************************************
/**	@defgroup lowerlevelservo Lower level servo drive methods and getters
*		@{
*/
/**	@defgroup lowerlevelservomeths Methods
*		@{
*/
/**	@brief	Overwrites the default settings taken from header file by the parameters given to this method
*		@param	pos_center				The postion at which the servo moves to straight forward. Default is 90. Allowed 10 <= pos_center <= 170.
*		@param	pos_left					The postion at which the servo moves to the left. Default is 170. Allowed 10 <= pos_center <= 170.
*		@param	pos_right					The postion at which the servo moves to the right. Default is 10. Allowed 10 <= pos_center <= 170.
*		@param	sweep_left_pos		If the servo is sweeping from left to the right - this defines the most left postion. Default is 140. Allowed 10 <= pos_center <= 170.
*		@param	sweep_right_pos		If the servo is sweeping from left to the right - this defines the most right postion. Default is 40. Allowed 10 <= pos_center <= 170.
*		@return	nothing
*/
void CodeRacer::servo_settings(uint8_t pos_center, uint8_t pos_left, uint8_t pos_right, uint8_t sweep_left_pos, uint8_t sweep_right_pos)
{
	servo_center_pos = pos_center;
	servo_left_pos = pos_left;
	servo_right_pos = pos_right;
	servo_sweep_left_pos = sweep_left_pos;
	servo_sweep_right_pos = sweep_right_pos;
}

/**	@brief	Turns sweeping of the servo from left to right and back on. 
*
*		The sweeping range is defind by #servo_sweep_left_pos and #servo_sweep_right_pos attributes. Both can be set by either servo_settings() or as public members.
*		Every time servo_sweep() is called the servo is driven by 5 steps until either #servo_sweep_left_pos or #servo_sweep_right_pos is reached. Then it will turn the
*		direction and step to the other side every time this method is called.
*		@return	nothing
*/
void CodeRacer::servo_sweep()
{
	uint8_t position;
	_servo_sweep = true;
	if (millis() - _servo_position_set_at_ms > SERVO_SWEEP_MS) {
		position = _servo_position + _servo_sweep_step;
		//sprintf(_debugmsg,"[%s] current position=%ld newpostion=%ld", __func__, _servo_position, position);
		if ((position >= servo_sweep_left_pos) || (position >= SERVO_MAX_POSITION)) {
			position = servo_sweep_left_pos;
			_servo_sweep_step = SERVO_SWEEP_TO_RIGHT_STEP;
		}
		if ((position <= servo_sweep_right_pos) || (position <= SERVO_MIN_POSITION)) {
			position = servo_sweep_right_pos;
			_servo_sweep_step = SERVO_SWEEP_TO_LEFT_STEP;
		}
		_servo_set_position(position);
	}
}

/**	@brief	Drives the servo to the postion that is defined by #servo_right_pos
*		@return	nothing
*/
void CodeRacer::servo_set_to_right()
{
	servo_set_position_wait(servo_right_pos);
}

/**	@brief	Drives the servo to the postion that is defined by #servo_left_pos
*		@return	nothing
*/
void CodeRacer::servo_set_to_left()
{
	servo_set_position_wait(servo_left_pos);
}

/**	@brief 	Drives the servo to the postion that is defined by #servo_center_pos
*		@return	nothing
*/
void CodeRacer::servo_set_to_center()
{
	servo_set_position_wait(servo_center_pos);
}

/**	@brief 	Drive the servo to the postion given to this method 
*
*		The method will wait until the servo has reached its new position.
*		@param position Position the servo will be drived to. Allowed are values 10<=postion<=170. 10 is at the right hand side, 170 at the left hand side.
*		@return	The new servo position
*/
uint8_t CodeRacer::servo_set_position_wait(uint8_t position)
{
	_servo_sweep = false;
	unsigned long wait_time_ms = _servo_set_position(position);
	delay(wait_time_ms);
	return(_servo_position);
}

/**	@brief Drive the servo to the postion given to this method 
*
*		The method will not wait until the servo has reached its new position.
*		@param position Position the servo will be drived to. Allowed are values 10<=postion<=170. 10 is at the right hand side, 170 at the left hand side.
*		@return	The time in ms the servo will need to reach the new position
*/
unsigned long CodeRacer::servo_set_position(uint8_t position)
{
	_servo_sweep = false;
	unsigned long wait_time_ms = _servo_set_position(position);
	return(wait_time_ms);
}

unsigned long CodeRacer::_servo_set_position(uint8_t position)
{
	uint8_t _position = position;
	uint8_t absdiff;

	if (position < SERVO_MIN_POSITION) {
		_position = SERVO_MIN_POSITION;
	}
	else if (position > SERVO_MAX_POSITION) {
		_position = SERVO_MAX_POSITION;
	}
	_servo->write(_position);
	// wait minimal delay to avoid code collaps
	delay(SERVO_SET_1TICK_POSITION_DELAY_MS); 
	absdiff = abs(_servo_position - _position);
	if (absdiff > 1) {
		_servo_position_eta_in_ms = absdiff * SERVO_SET_1TICK_POSITION_DELAY_MS;
	}
	else {
		_servo_position_eta_in_ms = 0;
	}
	
	_servo_position_set_at_ms = millis();
	_servo_position = _position;

	return(_servo_position_eta_in_ms);
}

/**	@} */ // end of group lowerlevelservomeths
/**	@defgroup lowerlevelservogetters Getters
*		@{
*/

/**	@brief Get the actual position of the servo
*		@return	Actual position of the servo
*/
uint8_t CodeRacer::servo_position() {
	return(_servo_position);
}

/**	@brief Get the system time in ms the servo was set to the actual position
*		@return	System time in ms the servo was set
*/
unsigned long CodeRacer::servo_position_set_at_ms() {
	return(_servo_position_set_at_ms);
}

/**	@brief Get the system time in ms the servo will reach its position
*		This is an estimated time. 
*		If this is a time in the future, the servo may still moving. 
*		If this is a time in the past , the servo should have reached its postion already.
*		@return	System time in ms the servo will reach its position
*/
unsigned long CodeRacer::servo_position_eta_in_ms() {
	return(_servo_position_eta_in_ms);
}

/**	@} */ // end of group lowerlevelservogetters
/**	@} */ // end of group lowerlevelservo


//**************************************
//*** Ultrasonic lower level control ***
//**************************************
/**	@defgroup lowerlevelus Lower level ultra sonic methods and getters
*		@{
*/
/**	@defgroup lowerlevelusmeths Methods
*		@{
*/

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in cm. 
*
*		This is the medium one out of 3 measurements. The maximum measured distance is about 100cm and defined by the US_MAX_ECHO_TIME_US setting in the header file.
*		@return	The measured distance in cm. 
*/
unsigned long CodeRacer::usonic_measure_cm()
{
	return(usonic_measure_cm(US_MAX_ECHO_TIME_US));
}

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in microseconds.
*
*		This is the medium one out of 3 measurements. The maximum measured distance is about 6000 microseconds and defined by the US_MAX_ECHO_TIME_US setting in the header file.
*		@return	The measured distance in microseconds. 
*/
unsigned long CodeRacer::usonic_measure_us()
	{
		return(usonic_measure_us(US_MAX_ECHO_TIME_US));
	}

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in cm.
*
*		This is the medium one out of 3 measurements. Be careful with high values for max_echo_run_time_us - this may increase run time due to the fact that if there is nothing in range of the sensor it will wait until this specified run time of the echo is over. 
*		The maximum range the sensor is specified for is about 300cm.
*		@param max_echo_run_time_us Defines the maximum echo run time and by that the maximum of distance that can be measured.
*		@return	The measured distance in cm. 
*/
unsigned long CodeRacer::usonic_measure_cm(unsigned long max_echo_run_time_us)
{
	unsigned long echo_runtime_us = usonic_measure_us(max_echo_run_time_us);
	unsigned long distance_cm = echo_runtime_us * 0.0172;
	//Serial.print("MEASURE_DISTANCE. Distance in cm is: ");
	//Serial.println(distance_cm);
	_usonic_distance_cm = distance_cm;
	return(distance_cm);
}

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in microseconds.
*
*		This is the medium one out of 3 measurements. Be careful with high values for max_echo_run_time_us - this may increase run time due to the fact that if there is nothing in range of the sensor it will wait until this specified run time of the echo is over.
*		The maximum range the sensor is specified for is about 300cm.
*		@param max_echo_run_time_us Defines the maximum echo run time in microseconds and by that the maximum of distance that can be measured.
*		@return	The measured distance in microseconds.
*/
unsigned long CodeRacer::usonic_measure_us(unsigned long max_echo_run_time_us)
{
	unsigned long echo_runtime_us[3] = { 0,0,0 };
	uint8_t measnr = 0;

	do {
		echo_runtime_us[measnr] = usonic_measure_single_shot_us(max_echo_run_time_us);
		if (echo_runtime_us[measnr] > 200) {
			measnr++;
		}
	} while (measnr < 3);

	// we will take the medium out of 3 values ... 
	if (echo_runtime_us[0] > echo_runtime_us[1]) { std::swap(echo_runtime_us[0], echo_runtime_us[1]); }
	if (echo_runtime_us[1] > echo_runtime_us[2]) { std::swap(echo_runtime_us[1], echo_runtime_us[2]); }
	if (echo_runtime_us[0] > echo_runtime_us[1]) { std::swap(echo_runtime_us[0], echo_runtime_us[1]); }

	//Serial.print("MEASURE_DISTANCE_US. Echo runtime in us is: ");
	//Serial.println(echo_runtime_us[1]);

	// if the stop at minimal distance is enabeled - check for minimal distance is reached
	if (true == _coderacer_stop_at_distance_enabled) {
		if (echo_runtime_us[1] <= _usonic_stop_distance_us) {
			_coderacer_stopped_at_min_distance = true;
			stop_driving();
			_coderacer_stop_at_distance_enabled = false;
		}
	}
	_usonic_distance_us = echo_runtime_us[1];
	return(echo_runtime_us[1]);
}

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in cm.
*
*		This is a one shot measurement. The maximum measured distance is about 6000 microseconds and defined by the US_MAX_ECHO_TIME_US setting in the header file.
*		@return	The measured distance in cm.
*/
unsigned long CodeRacer::usonic_measure_single_shot_cm()
{
	return(usonic_measure_single_shot_cm(US_MAX_ECHO_TIME_US));
}

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in microseconds.
*
*		This is a one shot measurement. The maximum measured distance is about 6000 microseconds and defined by the US_MAX_ECHO_TIME_US setting in the header file.
*		@return	The measured distance in microseconds.
*/
unsigned long CodeRacer::usonic_measure_single_shot_us()
{
	return(usonic_measure_single_shot_us(US_MAX_ECHO_TIME_US));
}

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in cm.
*
*		This is a one shot measurement. Be careful with high values for max_echo_run_time_us - this may increase run time due to the fact that if there is nothing in range of the sensor it will wait until this specified run time of the echo is over.
*		The maximum range the sensor is specified for is about 300cm.
*		@param max_echo_run_time_us Defines the maximum echo run time in microseconds and by that the maximum of distance that can be measured.
*		@return	The measured distance in cm.
*/
unsigned long CodeRacer::usonic_measure_single_shot_cm(unsigned long max_echo_run_time_us)
{
	// convert into cm ... 344m/sec is the speed of noise - thus 34400cm/sec ... or 34,400cm/milisec ... or 0,0344cm/microsec
	// the echo has to go the distance twice - forth and back - so the duration has to be the half of the measured one
	// distance_cm = echo_duration/2 * 0,0344   or   distance_cm = echo_duration/2 / 29,1 or distance_cm = echo_duration * 0,0172
	// distance_cm = (echo_duration/2) / 29.1;
	unsigned long echo_runtime_us = usonic_measure_single_shot_us(max_echo_run_time_us);
	unsigned long distance_cm = echo_runtime_us * 0.0172;
	//Serial.print("MEASURE_DISTANCE. Distance in cm is: ");
	//Serial.println(distance_cm);
	_usonic_distance_cm = distance_cm;
	return(distance_cm);
}

/**	@brief Measures the distance to the next object in front of the ultra sonic sensor in microseconds.
*
*		This is a one shot measurement. Be careful with high values for max_echo_run_time_us - this may increase run time due to the fact that if there is nothing in range of the sensor it will wait until this specified run time of the echo is over.
*		The maximum range the sensor is specified for is about 300cm.
*		@param max_echo_run_time_us Defines the maximum echo run time in microseconds and by that the maximum of distance that can be measured.
*		@return	The measured distance in microseconds.
*/
unsigned long CodeRacer::usonic_measure_single_shot_us(unsigned long max_echo_run_time_us)
	{
		unsigned long echo_runtime_us;
		// start measurement - send a short pulse "HIGH" to the TRIG pin of the ultrasonic sensor
		pinMode(_us_echo_pin, OUTPUT);
		pinMode(_us_echo_pin, INPUT);
		digitalWrite(_us_trigger_pin, LOW);
		delayMicroseconds(2);
		digitalWrite(_us_trigger_pin, HIGH);
		delayMicroseconds(10);
		digitalWrite(_us_trigger_pin, LOW);
		// measure runtime in microseconds until the ECHO pin aof the sensor goes HIGH
		echo_runtime_us = pulseInLong(_us_echo_pin, HIGH, max_echo_run_time_us);
		if (echo_runtime_us == 0) {
			echo_runtime_us = max_echo_run_time_us; // US_MAX_ECHO_TIME_US;
		}
		//Serial.print("MEASURE_DISTANCE_US. Echo runtime in us is: ");
		//Serial.println(echo_runtime_us);
		_usonic_distance_us = echo_runtime_us;
		return(echo_runtime_us);
	}

/**	@} */ // end of group lowerlevelusmethods
/**	@defgroup lowerlevelusgetters Setters and getters
*		@{
*/

/**	@brief Sets the stop distance in cm. 
*
*		If start_stop_at_min_distance() is used and distance measured with one of the measurement methods - the racer will be stopped immediately. All except the singe shot methods of the ultra sonic measurements methods supports that.
*		Internally the stop distance will be set as both - in cm and in microseconds.
*		@param stop_distance_cm Distance in cm the racer will be stopped if that features was enabled by start_stop_at_min_distance() before.
*		@return	nothing
*/
void CodeRacer::usonic_set_stop_distance_cm(unsigned long stop_distance_cm)
{
	_usonic_stop_distance_us = stop_distance_cm * 58.14;
}

/**	@brief Sets the stop distance in cm. 
*
*		If start_stop_at_min_distance() is used and distance measured with one of the measurement methods - the racer will be stopped immediately. All except the singe shot methods of the ultra sonic measurements methods supports that.
*		Internally the stop distance will be set as both - in cm and in microseconds.
*		@param stop_distance_us Distance in cm the racer will be stopped if that features was enabled by start_stop_at_min_distance() before.
*		@return	nothing
*/
void CodeRacer::usonic_set_stop_distance_us(unsigned long stop_distance_us)
{
	_usonic_stop_distance_us = stop_distance_us;
}

/**	@brief Returns the last measured distance in microseconds
*		@return	Distance in microseconds
*/
unsigned long CodeRacer::usonic_distance_us() {
	return(_usonic_distance_us);
}

/**	@brief  Returns the last measured distance in cm
*		@return	Distance in cm
*/
unsigned long CodeRacer::usonic_distance_cm() {
	return(_usonic_distance_cm);
}

/**	@} */ // end of group lowerlevelusgetters
/**	@} */ // end of group lowerlevelus


//**************************************
//*** Drives lower level control ***
//**************************************

/**	@defgroup lowerleveldrives Lower level drives methods and getters
*		@{
*/
/**	@defgroup lowerleveldrivesmeths Methods
*		@{
*/

/**	@brief Overwrites some drive settings. This will replace the defaults set by the values in the header file.
*		@param drive_left_speed	Speed of the left side drive
*		@param drive_right_speed Speed of the right side drive
*		@param turn_left_for_ms Time in ms the racer will turn to the left around its center if turn_left() is called
*		@param turn_right_for_ms Time in ms the racer will turn to the right around its center if turn_right() is called
*		@return	nothing
*/
void CodeRacer::drives_settings(uint8_t drive_left_speed, uint8_t drive_right_speed, unsigned long turn_left_for_ms, unsigned long turn_right_for_ms)
{
	_drive_left_speed = drive_left_speed;
	_drive_right_speed = drive_right_speed;
	_turn_left_for_ms = turn_left_for_ms;
	_turn_right_for_ms = turn_right_for_ms;
}

/**	@brief Stopps both drives
*		@return	nothing
*/
void CodeRacer::set_drives_stop_left_right() {
	set_drive_left_state(DRIVESTOP);
	set_drive_right_state(DRIVESTOP);
}

/**	@brief Sets both of the drives to a specified drivestate (DRIVESTOP, DRIVEFRWD, DRIVEBACK)
*		@param stateleft drivestate to set for the left side drive
*		@param stateright drivestate to set for the right side drive
*		@return	nothing
*/
void CodeRacer::set_drives_states_left_right(drivestate stateleft, drivestate stateright) {
	set_drive_left_state(stateleft);
	set_drive_right_state(stateright);
}

/**	@brief Sets the left side drive to the specified drivestate (DRIVESTOP, DRIVEFRWD, DRIVEBACK)
*		@param state drivestate to set for the left side drive
*		@return	nothing
*/
void CodeRacer::set_drive_left_state(drivestate state) {
	set_drive_state(state, _drive_left_frwd_pin, _drive_left_back_pin);
}

/**	@brief Sets the right side drive to the specified drivestate (DRIVESTOP, DRIVEFRWD, DRIVEBACK)
*		@param state drivestate to set for the right side drive
*		@return	nothing
*/
void CodeRacer::set_drive_right_state(drivestate state) {
	set_drive_state(state, _drive_right_frwd_pin, _drive_right_back_pin);
}

/**	@brief Sets the specified drivestate for the drive connected to the sepecified pins (DRIVESTOP, DRIVEFRWD, DRIVEBACK)
*		@param state drivestate to set for the connected drive
*   @param frwdpin	Pin the forward  signal of the drive device driver is connected at
*   @param backpin	Pin the backward  signal of the drive device driver is connected at
*		@return	nothing
*/
void CodeRacer::set_drive_state(drivestate state, uint8_t frwdpin, uint8_t backpin) {
	switch (state) {
	case DRIVESTOP:
		digitalWrite(frwdpin, LOW);
		digitalWrite(backpin, LOW);
		break;
	case DRIVEFRWD:
		digitalWrite(frwdpin, HIGH);
		digitalWrite(backpin, LOW);
		break;
	case DRIVEBACK:
		digitalWrite(frwdpin, LOW);
		digitalWrite(backpin, HIGH);
		break;
	}
}

/**	@brief	Sets the speed for both of the drives.
*
*		The drive will run with that speed afterwards. The direction of the drive has to be specfied with one of the set_drive_state methods before
*		@param speedleft speed of the left side drive. 0<=speed<=255
*		@param speedright speed of the right side drive. 0<=speed<=255
*		@return	nothing
*/
void CodeRacer::set_drives_speed_left_right(uint8_t speedleft, uint8_t speedright) {
	set_drive_left_speed(speedleft);
	set_drive_right_speed(speedright);
}

/**	@brief 	Sets the speed for the left side drive.
*
*		The drive will run with that speed afterwards. The direction of the drive has to be specfied with one of the set_drive_state methods before
*		@param  speed speed of the left side drive. 0<=speed<=255
*		@return	nothing
*/
void CodeRacer::set_drive_left_speed(uint8_t speed) {
	set_drive_speed(speed, _drive_left_enable_pin);
}

/**	@brief	Sets the speed for the right side drive.
*
*		The drive will run with that speed afterwards. The direction of the drive has to be specfied with one of the set_drive_state methods before
*		@param speed speed of the right side drive. 0<=speed<=255
*		@return	nothing
*/
void CodeRacer::set_drive_right_speed(uint8_t speed) {
	set_drive_speed(speed, _drive_right_enable_pin);
}

/**	@brief Sets the speed for the drive of the enable pin connected to the specified pin.
*
*		The drive will run with that speed afterwards. The direction of the drive has to be specfied with one of the set_drive_state methods before
*		@param speed speed of the drive. 0<=speed<=255
*		@param enablepin Pin the drives device driver enable pin is connected at
*		@return	nothing
*/
void CodeRacer::set_drive_speed(uint8_t speed, uint8_t enablepin) {
	_analog_write(enablepin, (int)speed);
}

/**	@} */ // end of group lowerleveldrivesmethods
/**	@defgroup lowerleveldrivesgetters Getters
*		@{
*/

/**	@brief Get the setting for the speed of the right side drive
*		@return	Speed of the right side drive
*/
uint8_t CodeRacer::drive_right_speed() {
	return _drive_right_speed;
}

/**	@brief Get the setting for the speed of the left side drive
*		@return	Speed of the left side drive
*/
uint8_t CodeRacer::drive_left_speed() {
	return(_drive_left_speed);
}

void CodeRacer::_analog_write(uint8_t pin, uint8_t speed) {
	if (pin == _drive_left_enable_pin) {
		_drive_left_speed = speed;
		ledcWrite(DRIVE_PWM_LEFT_CHANNEL, speed); 
	}
	if (pin == _drive_right_enable_pin) { 
		_drive_right_speed = speed;
		ledcWrite(DRIVE_PWM_RIGHT_CHANNEL, speed); 
	}
}

/**	@} */ // end of group lowerleveldrivesgetters
/**	@} */ // end of group lowerleveldrives


//**************************************
//*** LED lower level control ***
//**************************************
/**	@defgroup lowerlevelled Lower level LED methods
*		@{
*/
/**	@defgroup lowerlevelledmeths Methods
*		@{
*/

/**	@brief Sets all of the 4 LEDs to a ledstate (LEDON, LEDOFF)
*		@param leftled	set state of status left LED (most left yellow led)
*		@param stopled	set state of status stop LED (red led)
*		@param frwdled set state of status forward LED (green or blue led)
*		@param rightled set state of status right LED (most right yellow led)
*		@return	nothing
*/
void CodeRacer::set_leds_left_stop_frwd_right(ledstate leftled, ledstate stopled, ledstate frwdled, ledstate rightled) {
	digitalWrite(_led_left_pin, leftled);
	digitalWrite(_led_frwd_pin, frwdled);
	digitalWrite(_led_right_pin, rightled);
	digitalWrite(_led_stop_pin, stopled);
}

/**	@brief Sets all of the 4 LEDs to the same ledstate (LEDON, LEDOFF)
*		@param alleds set state to all status LEDs 
*		@return	nothing
*/
void CodeRacer::set_leds_all(ledstate alleds) {
	digitalWrite(_led_left_pin, alleds);
	digitalWrite(_led_frwd_pin, alleds);
	digitalWrite(_led_right_pin, alleds);
	digitalWrite(_led_stop_pin, alleds);
}

/**	@brief Sets all of the 4 LEDs to the ledstate LEDOFF
*		@return	nothing
*/
void CodeRacer::set_leds_all_off() {
	set_leds_all(LEDOFF);
}

/**	@brief Sets all of the 4 LEDs to the ledstate LEDON
*		@return	nothing
*/
void CodeRacer::set_leds_all_on() {
	set_leds_all(LEDON);
}

/**	@} */ // end of group lowerlevelledmets
/**	@} */ // end of group lowerlevelled


//**************************************
//*** ISRs ***
//**************************************
//IRAM_ATTR is used to load the code into DRAM and not to Flash to make it faster - required for ISRs
void IRAM_ATTR CodeRacer::_set_button_state() {
	if ((millis() - button_last_pressed_at_ms) > BUTTON_BOUNCING_TIME_MS) {
		button_last_pressed_at_ms = millis();			// simplest debouncing - just wait ;-)
		coderracer_activ = !coderracer_activ;
	}
}


//**********************************************************************************************************************
//**  Methods below this are obsolete and only in here for compatiblity to other projects - do not use them anymore !!!
//**********************************************************************************************************************
void CodeRacer::motor_einstellungen(uint8_t drive_left_speed, uint8_t drive_right_speed, unsigned long turn_left_for_ms, unsigned long turn_right_for_ms)
{
	drives_settings(drive_left_speed, drive_right_speed, turn_left_for_ms, turn_right_for_ms);
}

void CodeRacer::servo_einstellungen(uint8_t pos_center, uint8_t pos_left, uint8_t pos_right, uint8_t sweep_left_pos, uint8_t sweep_right_pos)
{
	servo_settings(pos_center, pos_left, pos_right, sweep_left_pos, sweep_right_pos);
}

void CodeRacer::servo_rechts()
{
	Serial.println("SERVO_RECHTS");																// Meldung am Monitor ausgeben
	servo_set_to_right();																					// Servo auf den Winkel rechts drehen
}

void CodeRacer::servo_links()
{
	Serial.println("SERVO_LINKS");																	// Meldung am Monitor ausgeben
	servo_set_to_left();																		// Servo auf den Winkel links drehen
}

void CodeRacer::servo_mitte()
{
	Serial.println("SERVO_MITTE");
	servo_set_to_center();
}

void CodeRacer::servo_schwenk()
{
	servo_sweep();
}

void CodeRacer::links()
{
	Serial.println("CODERACER_LINKS");
	turn_left();
}

void CodeRacer::rechts()
{
	Serial.println("CODERACER_RECHTS");
	turn_right();
}

void CodeRacer::anhalten()
{
	Serial.println("CODERACER_ANHALTEN");
	stop_driving();
}

void CodeRacer::vorwaerts()
{
	Serial.println("CODERACER_VORWAERTS");
	drive_forward();
}

void CodeRacer::rueckwaerts()
{
	Serial.println("CODERACER_RUECKWAERTS");
	drive_backward();
}

unsigned long CodeRacer::abstand_messen()
{
	return(0);
	
	unsigned long distance_cm = 0;
	unsigned long min_distance_cm = 1000;
	int8_t servo_turn_direction = 0;


	// while driving or sweeping there will be only one value be measured - else there will be mutiple measurements and the servor will be turning
	if (((true == _drive) || (servo_center_pos == _servo_position) || _servo_sweep == true)) {
		// while driving ...
		//Serial.println("ABSTAND_MESSEN im fahren, direkt nach vorn oder Schwenk() ist aktiv ...");
		min_distance_cm = usonic_measure_cm();
	}	else {
		// no sweep, not driving ...
		//Serial.println("ABSTAND_MESSEN im Stand nach links oder rechts...");
		// are we already ath left or right with the servo ?
		if (servo_left_pos == _servo_position) {
			//Serial.println("ABSTAND_MESSEN. Linke Seite.");
			//left ...
			servo_turn_direction = SERVO_SWEEP_TO_RIGHT_STEP;
		}
		if (servo_right_pos == _servo_position) {
			//right ...
			//Serial.println("ABSTAND_MESSEN rechte Seite.");
			servo_turn_direction = SERVO_SWEEP_TO_LEFT_STEP;
		}
		// trun the servo and do measuring ... remember the smallest value
		do {
			_servo_set_position(_servo_position + servo_turn_direction);
			//Serial.print("ABSTAND_MESSEN. Servo position:");
			//Serial.println(_servo_position);
			distance_cm = usonic_measure_cm();
			if (distance_cm < min_distance_cm) {
				min_distance_cm = distance_cm;
			}
		} while ((_servo_position > H_SERVO_CENTER_LEFT) || (_servo_position < H_SERVO_CENTER_RIGHT));
	}

	Serial.print("ABSTAND_MESSEN. Der Abstand in cm ist (mindestens):");
	Serial.println(min_distance_cm);
	//_min_distance_cm = min_distance_cm;

	return(min_distance_cm);

}

//**********************************************************************************************************************
//**  Helper methods
//**********************************************************************************************************************

