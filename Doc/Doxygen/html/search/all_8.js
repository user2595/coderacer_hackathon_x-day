var searchData=
[
  ['turn_5fleft',['turn_left',['../d2/d40/group__higherlevelmeths.html#ga86b7caf6ff46e9d1ad90ed507864b175',1,'CodeRacer::turn_left()'],['../d2/d40/group__higherlevelmeths.html#ga30e1ec3fbbc206f93ea66dbf91b5fd95',1,'CodeRacer::turn_left(unsigned long turn_for_ms)'],['../d2/d40/group__higherlevelmeths.html#gae6daa587199e5bf95b1aac675de53b0e',1,'CodeRacer::turn_left(unsigned long turn_for_ms, uint8_t left_speed, uint8_t right_speed)']]],
  ['turn_5fleft_5ffor_5fms',['turn_left_for_ms',['../d7/d45/group__higherlevelgetters.html#gaf04fd16ca0e2ace656f9549c43d16459',1,'CodeRacer']]],
  ['turn_5fright',['turn_right',['../d2/d40/group__higherlevelmeths.html#ga8969fb2d6e2b5ac44a99197931e6b8da',1,'CodeRacer::turn_right()'],['../d2/d40/group__higherlevelmeths.html#gae1f175aad98d773b0206f483ae0bb4ea',1,'CodeRacer::turn_right(unsigned long turn_for_ms)'],['../d2/d40/group__higherlevelmeths.html#gad10b3457489cc7e25ffb4d64c539528a',1,'CodeRacer::turn_right(unsigned long turn_for_ms, uint8_t left_speed, uint8_t right_speed)']]],
  ['turn_5fright_5ffor_5fms',['turn_right_for_ms',['../d7/d45/group__higherlevelgetters.html#gac0698f02f6a21d9d1f5b9cf2820306cf',1,'CodeRacer']]]
];
