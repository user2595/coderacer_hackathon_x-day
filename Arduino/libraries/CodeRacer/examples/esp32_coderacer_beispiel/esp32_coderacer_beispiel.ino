#include <CodeRacer.h>

//----- settings for the ultrasonic sensor -----
#define US_STOP_ABSTAND_CM 20     // if distance goes below that - stop the racer

//----- {CODES}RACER API -> online: https://doc.itsblue.de/Fenoglio/coderacer/Doku/Doxygen/html/
//-- Some main higher level methods listed below
// void CodeRacer::stop_driving ()                  Stops the racer and sets status LEDs
// void CodeRacer::drive_forward ()                 Sets the speed and the directions of both drives so that it will move forward. 
// void CodeRacer::drive_backward ()                Sets the speed and the directions of both drives so that it will move backward.
// void CodeRacer::turn_left ()                     Will turn the racer to the left for the internally stored time in ms and with the internally stored speed.
// void CodeRacer::turn_right ()                    Will turn the racer to the right for the internally stored time in ms and with the internally stored speed. 
// void CodeRacer::start_stop_at_min_distance ()    Enables to stopp the racer if during a distance measurement the measured distance is smaller then the internally stored minimal distance. 
// void CodeRacer::stop_stop_at_min_distance ()     Disables to stopp the racer if during a distance measurement the measured distance is smaller then the specified minimal distance. 
// bool CodeRacer::start_stop ()                    This will return if the codracer is in active mode or not. 
// void CodeRacer::servo_set_to_right ()            Drives the servo to the postion that is defined by #servo_right_pos.
// void CodeRacer::servo_set_to_left ()             Drives the servo to the postion that is defined by #servo_left_pos. 
// void CodeRacer::servo_set_to_center ()           Drives the servo to the postion that is defined by #servo_center_pos.
// uint8_t CodeRacer::servo_position ()             Get the actual position of the servo.
// unsigned long CodeRacer::usonic_measure_cm ()    Measures the distance to the next object in front of the ultra sonic sensor in cm. 
// 
// ... there are much more ... read the online API for more details.

//----- variables we need 
unsigned long distance_cm = 0; 

//---- construct the coderacer object
CodeRacer coderacer;

//---- set up code - executed ones
void setup() {
    // start serial monitor
    Serial.begin(115200);
    // initialize the coderacer
    coderacer.begin();
    // enable fun stuff
    coderacer.coderacer_fun_enabled = true;
    // look to the left, to the right and to center... :-)
    coderacer.servo_set_to_left();
    delay(100);
    coderacer.servo_set_to_right();
    delay(100);    
    coderacer.servo_set_to_center(); 
    delay(100);
}

//---- 'endless' loop  
void loop() {

  // check if the racer was started (button was toggled to coderacer active state 
  if(true == coderacer.start_stop()){

    Serial.print("Speed of right side drive: ");
    Serial.println(coderacer.drive_right_speed());
    Serial.print("Speed of left side drive: ");
    Serial.println(coderacer.drive_left_speed());

    // measure the distance - at the position of the servo 
    distance_cm = coderacer.usonic_measure_cm();  
         
    coderacer.start_stop_at_min_distance(US_STOP_ABSTAND_CM);
    while(!coderacer.stopped_at_min_distance()){

      Serial.print("Distanc in cm: ");
      Serial.println(distance_cm);    
      
      if(distance_cm > 50){
        coderacer.drive_forward();
        coderacer.servo_sweep();
      }
      else if(distance_cm > 40){
        coderacer.turn_right();        
      }
      else if(distance_cm > 30){
        coderacer.turn_left();
      }
      else { 

        coderacer.drive_backward();
      }

      // measure the distance - at the position of the servo 
      distance_cm = coderacer.usonic_measure_cm();  
      
    }
    
    Serial.println("***** STOPPED ***** ");
    Serial.print("Measured stop distanc of cm: ");
    Serial.println(distance_cm);    
    Serial.print("Measured at servo position of: ");
    Serial.println(coderacer.servo_position());

    coderacer.set_inactive();
    
  }
  
}



